import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoyagepageComponent } from './voyagepage.component';

describe('VoyagepageComponent', () => {
  let component: VoyagepageComponent;
  let fixture: ComponentFixture<VoyagepageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoyagepageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoyagepageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
