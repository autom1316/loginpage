import { Component, OnInit } from '@angular/core';
import voyageForm from 'src/assets/voyageForm.json';
import { orderBy,sortBy } from 'lodash';

@Component({

  selector: 'app-voyagepage',

  templateUrl: './voyagepage.component.html',

  styleUrls: [ './voyagepage.component.css' ]

})

export class VoyagepageComponent  implements OnInit  {

  name = 'Angular';

  page = 1;
  pageSize = 3;
  collectionSize = voyageForm.data.voyages.length;
  voyagedata: any = voyageForm.data.voyages;
  // this.voyagedata.sort = this.sort;
  // this.voyagedata.paginator = this.paginator;
  
  constructor() {
    this.refreshVoyages();
  }

  refreshVoyages() {
    this.voyagedata = voyageForm.data.voyages
      .map((voyagedata, i) => ({id: i + 1, ...voyagedata}))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  ngOnInit() {
  
    console.log (this.voyagedata);
  
}
}