import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';

import { FormComponent } from './form/form.component';

import { FormtwoComponent } from './formtwo/formtwo.component';

import { LoginComponent } from './login/login.component';

import { LandingComponent } from './landing/landing.component';

import { VoyagepageComponent } from './voyagepage/voyagepage.component';


VoyagepageComponent

const routes: Routes = [

  {path : 'login', component : LoginComponent},

  {path : 'form', component : FormComponent},

  {path : 'formtwo', component : FormtwoComponent},

  {path : 'landing', component : LandingComponent},

  {path : 'voyage', component : VoyagepageComponent}



];

@NgModule({

  imports: [RouterModule.forChild(routes)],

  exports: [RouterModule]

})

export class AdminRoutingModule { }

