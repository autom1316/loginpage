import { Component, OnInit} from '@angular/core';
import { ToastrService } from 'ngx-toastr';  
import { FormGroup, FormBuilder, RequiredValidator, Validators } from '@angular/forms';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  public form: FormGroup;
  title = 'angulartoastr';
  
  constructor( private formBuilder: FormBuilder, private toastr: ToastrService)
  {

    // this.form.get('country').valueChanges.subscribe(

    //   (country: string) => {
  
    //       if (country === 'USA') {
  
    //         this.form.get('post').setValidators([Validators.required, Validators.pattern('^[0-9]{5}(?:-[0-9]{4})?$')]);
  
    //       } else if (country === 'Canada') {
  
    //         this.form.get('post').setValidators([Validators.required, Validators.pattern('[ABCEGHJKLMNPRSTVXY][0-9][ABCEGHJKLMNPRSTVWXYZ] ?[0-9][ABCEGHJKLMNPRSTVWXYZ][0-9]')]);
  
    //       }
  
    //       this.form.get('post').updateValueAndValidity();
  
    //   })
    //   console.log("HI")

  }
       
  get country() {
    return this.form.get('country');
    
  }  
  get post() {
    return this.form.get('post');
  } 
  // tslint:disable-next-line: typedef
  logC(x){

    let countrySelected = this.form.get('country').value;
    if(countrySelected === 'Canada') {
      this.showStuff("zip")
      this.form.get('post').setValidators([
        Validators.required,
        Validators.pattern("[A-Z]+[0-9]+[A-Z]+[0-9]+[A-Z]+[0-9]")]); 
        this.form.get('post').updateValueAndValidity();
    }
     else if (countrySelected === 'USA') {
      this.showStuff("zip")
      this.form.get('post').setValidators([
        Validators.required,
        Validators.pattern("[0-9]{5}")]); 
        this.form.get('post').updateValueAndValidity();
    }
    else {
      this.hideStuff("zip")
      this.form.get('post').clearValidators(); 
      this.form.get('post').updateValueAndValidity();
    }
    
    // const postControl = this.form.get('post');
    // this.country.valueChanges.subscribe(
    //   country => {
    //          console.log("mode");
    //         if (country === 'Canada') {
    //           // postControl.setValidators([Validators.required]);
    //           postControl.setValidators([Validators.required, Validators.pattern('^[0-9]{5}(?:-[0-9]{4})?$')]);
    //         }
    //         else if (country === 'USA') {
    //           postControl.setValidators([Validators.required]);
                
    //         }

    //         else
    //       {

    //         postControl.clearValidators();
    //       }
    //       postControl.updateValueAndValidity();
    //     });

    // if (x.value === 'Canada') {
     

    //   console.log(x.value);
    //   console.log(this.form.get('post'));
    //   this.form.get('post').setValidators([Validators.required, Validators.pattern('[ABCEGHJKLMNPRSTVXY][0-9][ABCEGHJKLMNPRSTVWXYZ] ?[0-9][ABCEGHJKLMNPRSTVWXYZ][0-9]')]);
    //   this.form.get('post').updateValueAndValidity();
    // }
    // else if (x.value === 'USA') {

    //   this.form.get('post').setValidators([Validators.required, Validators.pattern('^[0-9]{5}(?:-[0-9]{4})?$')]);
    //   this.form.get('post').updateValueAndValidity();
    // }
    // else{
    //   this.form.get('post').clearValidators();
    //   this.form.get('post').updateValueAndValidity();
    // }


  }
  log(x) {

    // const postControl = this.form.get('post');
    // this.country.valueChanges.subscribe(
    //   country => {
    //          console.log("mode");
    //         if (country === 'Canada') {
    //           // postControl.setValidators([Validators.required]);
    //           postControl.setValidators([Validators.required, Validators.pattern('^[0-9]{5}(?:-[0-9]{4})?$')]);
    //         }
    //         else if (country === 'USA') {
    //           postControl.setValidators([Validators.required]);
                
    //         }

    //         else
    //       {

    //         postControl.clearValidators();
    //       }
    //       postControl.updateValueAndValidity();
    //     });

    // if (x.value === 'Canada') {

    //   console.log(x.value);
    //   console.log(this.form.get('post'));
    //   this.form.get('post').setValidators([Validators.required, Validators.pattern('[ABCEGHJKLMNPRSTVXY][0-9][ABCEGHJKLMNPRSTVWXYZ] ?[0-9][ABCEGHJKLMNPRSTVWXYZ][0-9]')]);

    // }
    // else if (x.value === 'USA') {

    //   this.form.get('post').setValidators([Validators.required, Validators.pattern('^[0-9]{5}(?:-[0-9]{4})?$')]);

    // }
  }

  showSuccess() {
    this.toastr.success('', 'SUCCESS',
    {timeOut: 2000});;
  }

  
  // tslint:disable-next-line: typedef
  onSubmit()  {
    console.log(this.form.value);
  }

  showStuff(id) {
    document.getElementById(id).style.display = 'block';
  }

  hideStuff(id) {
      document.getElementById(id).style.display = 'none';
  }


  ngOnInit() {

    // this.form = this.formBuilder.group({


    //     post: ['', [Validators.required, Validators.pattern('^[0-9]{5}(?:-[0-9]{4})?$')]],

    //     country: ['US']

    // });
}
}